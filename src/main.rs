#[cfg(test)]
mod test;

fn main() {
	let result = FizzBuzzer::get()
		.from(1)
		.to(100)
		.with((3, "Fizz"))
		.with((5, "Buzz"))
		.with((7, "Bam"))
		.with((11, "Bang"))
		.go();
	println!("{}", result.join("\n"))
}

mod fizzbuzz;
pub use fizzbuzz::*;
