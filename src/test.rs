use super::*;

#[test]
fn test_basic() {
	let result = FizzBuzzer::get().go();
	assert_eq!(
		result,
		vec![
			"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14",
			"FizzBuzz",
		]
	)
}
#[test]
fn test_verbose() {
	let result = FizzBuzzer::get()
		.with((3, "Fizz"))
		.with((5, "Buzz"))
		.from(1)
		.to(15)
		.go();

	assert_eq!(
		result,
		vec![
			"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14",
			"FizzBuzz",
		]
	)
}
