pub type Fizzism = (usize, &'static str);

pub struct FizzBuzzer {
	fizzisms: Vec<Fizzism>,
	cap: usize,
	start: usize,
}

impl FizzBuzzer {
	pub fn get() -> FizzBuzzer {
		FizzBuzzer {
			cap: 15,
			start: 1,
			fizzisms: Vec::new(),
		}
	}
	pub fn from(&mut self, i: usize) -> &mut FizzBuzzer {
		self.start = i;
		self
	}
	pub fn to(&mut self, i: usize) -> &mut FizzBuzzer {
		self.cap = i;
		self
	}
	pub fn with(&mut self, i: Fizzism) -> &mut FizzBuzzer {
		self.fizzisms.push(i);
		self
	}
	pub fn go(&mut self) -> Vec<String> {
		if self.fizzisms.len() == 0 {
			self.fizzisms.push((3, "Fizz"));
			self.fizzisms.push((5, "Buzz"));
		}
		let mut out = Vec::new();
		for i in self.start..=self.cap {
			let mut line: Vec<String> = Vec::new();
			for fizzism in &self.fizzisms {
				if i % fizzism.0 == 0 {
					line.push(fizzism.1.to_string());
				}
			}
			if line.len() == 0 {
				line.push(i.to_string());
			}
			out.push(line.join(""))
		}
		out
	}
}
